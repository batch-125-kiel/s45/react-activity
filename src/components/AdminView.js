import React, {useState, useEffect} from 'react';
import {Container, Table, Button, Modal, Form} from 'react-bootstrap';


						//props contains course array and fetchdata function
export default function AdminView(props){

	//destructred, fetchData btw is a function
	const {courseData, fetchData} = props;

	//states
	const [courseId, setCourseId] = useState('');
	const [courses, setCourses] = useState([]);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);


	let token = localStorage.getItem('token');

	const openAdd = ()=> setShowAdd(true);
	const closeAdd = ()=> setShowAdd(false);


	//function in the admin buttons
	const openEdit = (courseId)=>{

		//get single course details to display in edit form
		fetch(`http://localhost:4000/api/courses/${courseId}`,{

			method: "GET",
			headers:{

				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`

			}
		})
		.then(result=> result.json())
		.then(result=>{

			//this rerun the fetch data so data refreshes asynchronously
			fetchData();

			//this set state in the edit form so current db data is displayed
			setCourseId(result._id);
			setName(result.name);
			setDescription(result.description);
			setPrice(result.price);

		})

		//---------------------
		setShowEdit(true);

	}

//
	const closeEdit = ()=>{

		setShowEdit(false);
		setName("");
		setDescription("")
		setPrice(0)
	}

	useEffect( () => {
		const coursesArr = courseData.map( (course) => {
			console.log(course)
			return(
				<tr key={course._id}>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td>
						{
							(course.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
					<td>
						<Button variant="primary" size="sm" 
						onClick={ ()=> openEdit(course._id) }>
							Update
						</Button>&nbsp;

						{
							(course.isActive === true) ?
								<Button variant="danger" size="sm"
								onClick={()=> archiveToggle(course._id, course.isActive)}>
									Archive
								</Button>
							:
								<Button variant="success" size="sm"
								onClick={ () => archiveToggle(course._id, course.isActive)}>
									Unarchive
								</Button>
						}

					</td>
				</tr>
			)
		})

		setCourses(coursesArr)
	}, [courseData])



	/*edit course function*/

	const editCourse =(e, courseId)=>{

		e.preventDefault();

		fetch(`http://localhost:4000/api/courses/edit/${courseId}`,{

		method: "PUT",
		headers: {

			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price

		})

		})
		.then(result=> result.json())
		.then(result =>{

			//WHAT IS THIS?
				//this autorefreshes the result data in the backgroundd
			fetchData();

			if(typeof result !== "undefined"){

				alert("Success");


				closeEdit();
			}else{

				alert("error");

			}

			
		})
	}


	const archiveToggle = (courseId, isActive)=>{

		fetch(`http://localhost:4000/api/courses/archive/${courseId}`,{


			method: "PUT",
			headers: {

				"Content-Type":"application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive:isActive
			})
		})
		.then(result => result.json())
		.then(result => {

			fetchData();

			if(result === true){

				alert('Success')
			}else{

				alert('Error');


			}
			
		})

	}

	const addCourse = (e)=>{

		e.preventDefault();

		fetch(`http://localhost:4000/api/courses/add`,{

			method: "POST",
			headers:{

				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price
			})
		})

		.then(result=>result.json())
		.then(result=>{

			console.log(result);

			if(result === true){

				fetchData()

				alert('Success');

				setName("");
				setDescription("")
				setPrice(0)

				closeAdd();
			}else{

				fetchData();

				alert('Error')
			}
		})
	}

	return (

		<Container>
			<div>
				<h2>Admin Dashboard</h2>
			<div>
			<Button variant="primary" onClick={openAdd}>Add new Course</Button>
			</div>
			</div>
			<Table>
				<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Availability</th>
					<th>Action</th>
				</tr>
					</thead>
					<tbody>

					{courses}

					</tbody>
			</Table>


			<Modal show={showEdit} onHide={closeEdit}>

			<Form onSubmit={(e)=>editCourse(e, courseId)}>
				<Modal.Header>
					<Modal.Title>
						Edit Course
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group controlId ="courseName">
						<Form.Label>Name</Form.Label>
						<Form.Control
						type="text"
						value={name}
						onChange={(e)=> setName(e.target.value)}
						/>
					</Form.Group>

					<Form.Group controlId ="courseDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control
						type="text"
						value={description}
						onChange={(e)=> setDescription(e.target.value)}
						/>
					</Form.Group>

					<Form.Group controlId ="coursePrice">
						<Form.Label>Price</Form.Label>
						<Form.Control
						type="text"
						value={price}
						onChange={(e)=> setDescription(e.target.value)}
						/>
					</Form.Group>

				</Modal.Body>

				<Modal.Footer>
				<Button variant = "secondary" onClick={closeEdit}>Close</Button>
				<Button variant = "success" type="submit">Submit</Button>
				</Modal.Footer>
				
			</Form>		

			</Modal>


		{/*ADD COURSE MODAL*/}
		<Modal show={showAdd} onHide={closeAdd}>
			
			<Form onSubmit ={(e)=>addCourse(e)}>
				
				<Modal.Header>
					Add Course
				</Modal.Header>

				<Modal.Body>

					<Form.Group courseId="courseName">
						<Form.Label>Name</Form.Label>
						<Form.Control

						type="text"
						value={name}
						onChange={(e)=>setName(e.target.value)}
						/>

					</Form.Group>
					<Form.Group courseId="courseDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control

						type="text"
						value={description}
						onChange={(e)=>setDescription(e.target.value)}

						/>
					</Form.Group>

					<Form.Group courseId="coursePrice">
						<Form.Label>Price</Form.Label>
						<Form.Control

						type="number"
						value={price}
						onChange={(e)=>setPrice(e.target.value)}

						/>
					</Form.Group>
				</Modal.Body>
				<Modal.Footer>

				<Button variant="secondary" onClick={closeAdd}>Close</Button>
				<Button variant="success" type="submit">Submit</Button>
					
				</Modal.Footer>
			</Form>
		</Modal>

		</Container>

		)
}