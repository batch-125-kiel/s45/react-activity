import {React, useState, useEffect} from 'react';
import {
Container,
Button

}from 'react-bootstrap'

export default function Counter(){

	const [count, setCount] = useState(0);

	//runs guaranteed once when the page loads
	useEffect(()=>{

		document.title = `You clicked ${count} times`

	}, [count]); //state listener, I removed this but still works

return (

	<Container className="m-4">

	<h1>You clicked {count} times</h1>
	<Button className="btn-warning" onClick={()=>{

		setCount(count+1)

	}}>Click Me</Button>
		
	</Container>
	)
}


