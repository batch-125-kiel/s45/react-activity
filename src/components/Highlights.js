import React from 'react';

import {
	Container,
	Row,
	Col,
	Card,
	Button

} from 'react-bootstrap/';




export default function Highlights(){

	return(

		<Container>
		<Row>
			<Col xs={12} md={4}>
			<Card>
			  <Card.Body>
			    <Card.Title>Learn from home</Card.Title>
			    <Card.Text>
			     Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cumque neque repellat fugit deserunt at nostrum, voluptas, doloremque animi voluptate modi, magni ipsum dolorum id fuga aliquid corporis? Molestias, consequatur, quibusdam.

			    </Card.Text>
			   
			  </Card.Body>
			</Card>
				
			</Col>

			<Col xs={12} md={4}>

			<Card>
			  <Card.Body>
			    <Card.Title>Study now, pay later.</Card.Title>
			    <Card.Text>
			     Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cumque neque repellat fugit deserunt at nostrum, voluptas, doloremque animi voluptate modi, magni ipsum dolorum id fuga aliquid corporis? Molestias, consequatur, quibusdam.

			    </Card.Text>
			  
			  </Card.Body>
			</Card>
				
			</Col>

			<Col xs={12} md={4}>

			<Card>
			  <Card.Body>
			    <Card.Title>Be Part of the Community</Card.Title>
			    <Card.Text>
			    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cumque neque repellat fugit deserunt at nostrum, voluptas, doloremque animi voluptate modi, magni ipsum dolorum id fuga aliquid corporis? Molestias, consequatur, quibusdam.

			    </Card.Text>
			    
			  </Card.Body>
			</Card>

			
				
			</Col>
		</Row>
		</Container>


	
		)

}