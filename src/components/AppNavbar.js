import React, {Fragment, useContext} from 'react';
/*React bootstrap*/
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink, useHistory, Redirect} from 'react-router-dom'
import UserContext from './../UserContext';




/*app navbar*/
export default function AppNavbar(){
/*
let user = props.user;*/

//import user only
const {user, unsetUser}= useContext(UserContext);


//useHistory a react dom router
let history = useHistory();

const logout = ()=>{

  unsetUser();
  history.push('/login')

}

const token =  localStorage.getItem('token');

//why not token here
let leftNav = (user.id !== null) ?
(user.isAdmin === true) ?
<Fragment>
<Nav.Link as={NavLink} to ="/addCourse">Add Course</Nav.Link>
<Nav.Link onClick={logout}>Logout</Nav.Link>
</Fragment>
:
<Fragment>
<Nav.Link onClick={logout}>Logout</Nav.Link>
</Fragment>

}



  return (

    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to ="/">Zuitter</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="">
        {/*//chuii navlink vs link*/}
          <Nav.Link as = {NavLink} to="/">Home</Nav.Link>
          <Nav.Link as = {NavLink} to="/courses">Courses</Nav.Link>
        </Nav>

        <Nav className="">
        {leftNav}
        </Nav>
      </Navbar.Collapse>
    </Navbar>

    )
}
