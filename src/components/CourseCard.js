import React from 'react';

import {
	Container,
	Row,
	Col,
	Card,
	Button

} from 'react-bootstrap/';


export default function CourseCard(){


	return (

		<Container className="mt-2 mb-5">

			<Row>
			<Col>
			<Card>
			
			  <Card.Body>
			    <Card.Title>Sample Course</Card.Title>
			    <Card.Text>
			   <h6>Description:</h6>
			   This is a sample course offering.
			    </Card.Text>
			     <Card.Text>
			    <h6>Price:</h6>
			    40, 000
			     </Card.Text>
			    <Button variant="primary">Enroll</Button>
			  </Card.Body>
			</Card>
			</Col>
			</Row>

			</Container>



		)
}

