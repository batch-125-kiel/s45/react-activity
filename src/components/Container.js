import React,{useState, useEffect} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom'



export default function Container (){


	return (

		<div className="m-5">

		<h1> ERROR 404- Not found</h1>
		<p> The page you are looking for cannot be found</p>

		<Link as = {NavLink} to="/">Back to home page</Link>

		</div>
		)

}