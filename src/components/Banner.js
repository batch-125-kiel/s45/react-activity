import React from 'react';

/*react bootstrap components*/
import {
	Container,
	Row,
	Col,
	Jumbotron,
	Button

} from 'react-bootstrap/';



export default function Banner(){

	return (

		<Container className="mt-3">
			<Row>

				<Col className="p-0">
					
					<Jumbotron className="p-3 bg-white">
					  <h1>Zuitt Coding Bootcamp</h1>
					  <p>
					   Opportunities for everyone
					  </p>
					  <p>
					    <Button variant="primary">Learn more</Button>
					  </p>
					</Jumbotron>
				</Col>

			</Row>
	
		</Container>


		//className for react since class is a reserve word
	/*	<div className="container-fluid">
		<div className="row justify-content-center">
			<div className = "col-10 col-md-8">
				<div className="jumbotron">
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere</p>
					<button className="btn btn-primary">Enroll</button>	
				</div>

			</div>
			
		</div>
		</div>*/


		)
}