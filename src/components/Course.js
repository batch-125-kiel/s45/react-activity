import {React, useState, useEffect}from 'react';
//proptypes validate whether component receive values
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';


import {
	Container,
	Row,
	Col,
	Card,
	Button

} from 'react-bootstrap/';




//this is the one called in Courses.js
							//courseProp came from Courses.js passed as a prop
							//destructured again
export default function Course({courseProp}){

	//destructure course prop
		//short cut or else courseProp.name...
	const {_id,name, description, price} = courseProp;


	//let course = props.course

	//state
		//[initial value, set value function]
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(10);
	const [isDisabled, setIsDisabled] = useState(false);



/*	function enroll(){


		if(seat>0){

			setSeat(seat-1)
			setCount(count +1)
			
		}else{

			//setIsDisabled( isDisabled= true)
			//alert('No more seats');
		}

		count < 30 ?
		setCount(count +1)
		setSeat(seat -1)
		:
		alert('No more slots');

	}
*/

//find difference useState vs useEffect
/*	useEffect(()=>{

		if(seat === 0){


			setIsDisabled(true);
		}

	},[seat])*/


	return (
		<Container className="mt-5">

		<Card className="mb-3">
		  <Card.Body>
		{/*data within braces are from destructured courseProp*/}
		    <Card.Title>{name}</Card.Title>  
		 	<h6>Description:</h6>
		   <p>{description}</p>
		    <h6>Price:</h6>
		    <p>{price}</p>
		    {/*<h5>Enrollees</h5>
		    <p>{count} Enrollees</p>
		    <p>{seat} Seats</p>
		     <br/>
		    <Button variant="primary" onClick={enroll} disabled={isDisabled}>Enroll</Button>*/}
		 
		   <Link className="btn btn-primary" to={`/courses/${_id}`}>View Course</Link>
		  </Card.Body>
		</Card>
		</Container>


		    /*()=>{
		    	count < 30 ?
		    	setCount(count+1)    	
		    	:
		    	alert('NO more slots') 
		    }*/


		)
}


//wtf is this again?
Course.propTypes = {

	course: PropTypes.shape({

		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}