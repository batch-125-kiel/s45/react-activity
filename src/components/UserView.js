import React, {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import Course from './Course'

								//desctruture
export default function UserView({courseData}){


	const [courses, setCourses] = useState([])

	useEffect(()=>{

		const coursesArr = courseData.map(course=>{

			if(course.isActive === true){


											//passing course prop to component Course
				return <Course key={course._id}courseProp={course}/>


			}else{

				return null
			}


		})

		setCourses(coursesArr);



	}, [courseData])

	return (

		<Container>

		{/*Display courses finally*/}

		{courses}
	
		</Container>

		)
}