import {React, useState, useEffect} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
/*import CourseCard from './components/CourseCard'*/
import Home from './pages/Home';
import Courses from './pages/Courses';
/*import Counter from './components/Counter'*/
import Register from './pages/Register'
import Login from './pages/Login'
import Container from './components/Container';
/*Context*/
import UserContext from './UserContext';
import SpecificCourse from './pages/SpecificCourse';


export default function App(){


const [user, setUser] = useState({

						id: null,
						isAdmin: null

});

//function to a variable so can be passed as context
const unsetUser = ()=>{

	localStorage.clear();
	//setted to null and logout problem solved, now redirect to login after logout
	setUser({
		id: null,
		isAdmin: null
	})
	/*setUser({email: null})*/
	//chuii if stil null without setting back to null after logout

}

//we're doing this because of being logged out but still with token ekeke
useEffect(()=>{

	let token = localStorage.getItem('token');

	fetch('http://localhost:4000/api/users/details',{

		method: "GET",
		headers: {

			"Authorization": `Bearer ${token}`
		}


	})
	.then(result=>result.json())
	.then(result=>{


		if(typeof result._id !=="undefined"){

			setUser({

				id:result._id,
				isAdmin:result.isAdmin
			})

		}else{


			setUser({

				id:null,
				isAdmin:null
			})

		}
	})



})

	return (

		<UserContext.Provider value={{user, setUser, unsetUser}}>

	{/*	//chuii*/}
			<BrowserRouter>
				{/*passing prop to appnavbar.js*/}
				<AppNavbar/>
				<Switch>

				
				<Route exact path ="/" component ={Home}/>
				<Route exact path ="/courses" component ={Courses}/>
				<Route exact path ="/register" component ={Register}/>
				<Route exact path ="/login" component ={Login}/>
				<Route exact path ="/courses/:courseId" component ={SpecificCourse}/>
				<Route component ={Container}/>

				</Switch>
			</BrowserRouter>
		</UserContext.Provider>

	
		)
}