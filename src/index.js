import {React, Fragment} from 'react';
import ReactDOM from 'react-dom';
/*Bootstrap*/
import 'bootstrap/dist/css/bootstrap.min.css';

/*Component*/

import App from './App.js';


ReactDOM.render(

  <App/>,

  document.getElementById('root')
);

