import {React, useState, useEffect, useContext} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import {useHistory, Redirect} from 'react-router-dom'


import UserContext from './../UserContext';

export default function Register(){

	const {user} = useContext(UserContext);

	//setting initial value
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const[email, setEmail] = useState('');
	const[password, setPassword] = useState('');
	const[verifyPassword, setVerifyPassword] = useState('');
	const[isDisabled, setIsDisabled] = useState(true);

	let history = useHistory();
	

	useEffect(()=>{

		if(email !== "" && password !=="" && verifyPassword !==""){

			setIsDisabled(false);
		}else{

			setIsDisabled(true);
		}

	},[email, password, verifyPassword]);//chuii


	function register (e){

		//console.log(email);

		e.preventDefault();

		fetch('http://localhost:4000/api/users/checkEmail',{

			method: "POST",
			headers: {

				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				email: email

			})
		
		})
			.then(result=>result.json())
			.then(result =>{

				if(result){

					alert('email already exists');

				}else{

					fetch('http://localhost:4000/api/users/register',{

						method: "POST",
						headers: {

							"Content-Type": "application/json"
						},
						body: JSON.stringify({

							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password

						})

					})
					.then(result =>result.json())
					.then(result =>{

						if(result == true){

							alert('Succesfully registered. Redirecting you to login.');

							setEmail('');
							setPassword('');
							setVerifyPassword('');

							history.push('/login')
						}else{


							alert('Something went wrong');
						}


					})


				}


			})


		/*return <Redirect to ="/login"/>*/

		
	}


	return (

		(user.id !== null) ?

		<Redirect to="/"/>

		:

		<Container className="mb-5">
			
			<h1 className="text-center">Register</h1>
		{/*	//right now is call back function register, chuii e*/}
			<Form  onSubmit={(e)=>register(e)}>
			  <Form.Group className="mb-3" controlId="formfirstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={(e)=>setFirstName(e.target.value)}/>
																	{/*chuii value*/}
			  </Form.Group>

			    <Form.Group className="mb-3" controlId="formlastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={(e)=>setLastName(e.target.value)}/>
																	{/*chuii value*/}
			  </Form.Group>

			    <Form.Group className="mb-3" controlId="formmobileNo">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control type="text" placeholder="Enter mobile number" value={mobileNo} onChange={(e)=>setMobileNo(e.target.value)}/>
																	{/*chuii value*/}
			  </Form.Group>

			    <Form.Group className="mb-3" controlId="formemail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e)=>setEmail(e.target.value)}/>
																	{/*chuii value*/}
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formpassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value={password} onChange={(e)=>setPassword(e.target.value)}/>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formpassword2">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Verify Password" value={verifyPassword} onChange={(e)=>setVerifyPassword(e.target.value)}/>
			  </Form.Group>

			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Submit
			  </Button>
			</Form>

		</Container>
	

		)

}





