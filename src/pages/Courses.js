import React, {useState, useEffect, useContext} from 'react';

/*Components*/
import Course from './../components/Course';
/*MOck data*/
/*import courses from './../mock-data/courses';*/
//React bootstrap
import {Container} from 'react-bootstrap';

import AdminView from './../components/AdminView.js'
import UserView from './../components/UserView.js'

import UserContext from './../UserContext';

export default function Courses (){


	let token = localStorage.getItem('token');

//
	const [courses, setCourses] = useState([]);
	const {user} = useContext(UserContext);

//this is where the course data all start
	const fetchData =()=>{

			fetch(`http://localhost:4000/api/courses/all`,{

				method: "GET",
				headers:{

					"Authorization": `Bearer ${token}`
				}
			})
			.then(result=>result.json())
			.then(result=>{

				//set state courses to result from fetch
				setCourses(result);

			})

	}

//chuii why is this here? what purpose? this seems to be the invocator of fetchdata
	useEffect(()=>{

		fetchData()

	}, [])


	return (

		<Container className="p-4">
			{(user.isAdmin === true)
				?
														//wut is this fetchdata ekekek
				<AdminView courseData = {courses} fetchData = {fetchData}/>

				:
				//pass the state courses with result data from fetch as prop to component userView
				<UserView courseData={courses}/>
				//User view component contains another component, the component for each course "Course.js"


			}	
	
		</Container>

		)




/*
	fetch(`http:localhost:4000/api/courses/all`,{

		method: "GET",
		headers:{

			"Authorization": `Bearer ${token}`
		}


	})
	.then(result=> result.json())
	.then(result=>{

		console.log(result);

		//console.log("TESTINGGNGGNGNGNG");

	let CourseCards = result.map(course=>{

		console.log(course);


			
			return <Course key={course.id} course ={course}/>
		})


	})*/


/*	return (

		<Container fluid>

			{CourseCards}
	
		</Container>



		)*/
}
