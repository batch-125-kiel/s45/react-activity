import React, {useState, useEffect, useContext} from 'react';
import {Container,Form,Button} from 'react-bootstrap';

/*Context import setUser from App.js via UserContext.js basta*/
import UserContext from './../UserContext';
import {Redirect} from 'react-router-dom'


export default function Login(){


	const[email, setEmail] = useState('');
	const[password, setPassword] = useState('');
	const[isDisabled, setIsDisabled] = useState(true);
	//useContext is a react hook
		//destructoring object
	const {user, setUser} = useContext(UserContext);


		useEffect(()=>{

		if(email !== "" && password !==""){

			setIsDisabled(false);
		}else{

			setIsDisabled(true);
		}

	},[email, password]);



	function login(e){

		e.preventDefault();

		fetch('http://localhost:4000/api/users/login',{

			method: "POST",
			headers: {

				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				email: email,
				password: password
			})

		})
		.then(result => result.json())
		.then(result =>{

			if(typeof result.access !=="undefined"){

				localStorage.setItem('token', result.access);
				userDetails(result.access);

			}

		})

		const userDetails =(token)=>{

			console.log("PUMASOK USER DETAILS")

			fetch('http://localhost:4000/api/users/details',{

				method: "GET",
				headers: {

					"Authorization": `Bearer ${token}`
				}
			})
			.then(result=> result.json())
			.then(result =>{


				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				});

//miss joy did not save this in storage
			/*	localStorage.setItem('id', result._id);
				localStorage.setItem('isAdmin', result.isAdmin);
*/
				alert('Login successful');

			})
		}


		//chuii passed as object here
		/*setUser({email: email});*/

		//local storage save
		/*localStorage.setItem('email', email);*/

		setEmail('');
		setPassword('');

	}

/*
	if(user.email !== null){

		return <Redirect to ="/"/>
	}
*/




	function k(e){

		setEmail(e.target.value);

	}



return (

		(user.id !== null) ?

		<Redirect to="/"/>

		:

	<Container className="mb-5">
	
	<h1 className="text-center">Login</h1>

	<Form  onSubmit={(e)=>login(e)}>
	  <Form.Group className="mb-3" controlId="formBasicEmail">
	    <Form.Label>Email address</Form.Label>
	    <Form.Control type="email" placeholder="Enter email" value={email} onChange={k}/>
															{/*chuii value*/}
	  </Form.Group>

	  <Form.Group className="mb-3" controlId="formBasicPassword">
	    <Form.Label>Password</Form.Label>
	    <Form.Control type="password" placeholder="Password" value={password} onChange={(e)=>setPassword(e.target.value)}/>
	  </Form.Group>


	  <Button variant="primary" type="submit" disabled={isDisabled}>
	    Submit
	  </Button>
	</Form>

</Container>

)

}
