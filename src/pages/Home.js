import React from 'react';

import {
	Container,
	Row,
	Col,
	Card,
	Button

} from 'react-bootstrap/';

/*Components*/

import Banner from './../components/Banner';
import Highlights from './../components/Highlights';


export default function Home (){

	return (
		<Container fluid>
		<Banner/>
		<Highlights/>
		</Container>


		)

}