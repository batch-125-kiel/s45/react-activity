import React, {useContext, useEffect, useState} from 'react';
import UserContext from './../UserContext'
import {Link, useParams, useHistory} from 'react-router-dom';
import {Container, Button, Card} from 'react-bootstrap';


export default function SpecificCourse(){

	const[name, setName] = useState('');
	const[description, setDescription] = useState('');
	const[price, setPrice] = useState(0);

	const {user} = useContext(UserContext);

	const {courseId} = useParams();
	let token = localStorage.getItem('token');
	let history = useHistory();

	useEffect(()=>{

		fetch(`http://localhost:4000/api/courses/${courseId}`,{

			headers:{
				"Authorization": `Bearer ${token}`
			}

		})
		.then(result=> result.json())
		.then(result=>{

			setName(result.name);
			setDescription(result.description);
			setPrice(result.price);

		})
	}, [])


	const enroll = ()=>{

		fetch(`http://localhost:4000/api/users/enroll`,{

			method: "POST",
			headers:{

				"Content-Type":"application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({

				courseId: courseId
			})
		})
		.then(result=>result.json())
		.then(result=> {

			console.log(result);

			if(result === true){

				alert('Enroll success');
				history.push('/courses');
			}else{

				alert('Error');
			}
		})
	}

	return(
		<Container>
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>
						{description}
					</Card.Text>
					<h6>
						Price: Php

						&nbsp;{price}
					</h6>	
				</Card.Body>
				<Card.Footer>
			{/*curly braces for conditional rendering*/}
					{
						(user.id !== null)
						?
						<Button variant ="primary" onClick={()=> enroll()}>Enroll</Button>
						:
						<Link className="btn btn-danger" to="/login">Login to Enroll</Link>
					}
				</Card.Footer>

			</Card>

		</Container>

		)
}